import { useState, useEffect } from 'react';

const max = 500;
const increment = 20;

const Scroll = () => {
  const [loading, setLoading] = useState(false);
  const [count, setCount] = useState(increment);

  const handleScroll = () => {
    if (
      window.innerHeight + document.documentElement.scrollTop !==
        document.documentElement.offsetHeight ||
      loading
    ) {
      return false;
    }

    setLoading(true);
  };

  useEffect(() => {
    if (!loading) return;

    if (count + increment >= max) {
      setCount(max);
    } else {
      setCount(count + increment);
    }

    setLoading(false);
  }, [loading]);

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);
    return () => window.removeEventListener('scroll', handleScroll);
  }, []);

  return { count };
};

export default Scroll;
