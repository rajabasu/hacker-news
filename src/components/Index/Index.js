import React, { useState, useEffect } from 'react';
import {
  getStoryIds,
  getTopStoryIds,
  getJobStoryIds,
} from '../../apicalls/HackerNewsApi';
import './Index.scss';
import Articles from '../Articles/Articles';
import Scroll from '../../utils/Scroll';
import classNames from 'classnames';
import { CircleArrow as ScrollUpButton } from 'react-scroll-up-button';

const Index = () => {
  const [storyIds, setStoryIds] = useState([]);
  const [active, setActive] = useState('newarticle');
  const { count } = Scroll();

  useEffect(() => {
    getStoryIds().then(({ data }) => setStoryIds(data));
  }, []);

  function setActiveArticle(val) {
    setActive(val);
    if (val === 'toparticle') {
      getTopStoryIds().then(({ data }) => setStoryIds(data));
    } else if (val === 'newarticle') {
      getStoryIds().then(({ data }) => setStoryIds(data));
    } else if (val === 'job') {
      getJobStoryIds().then(({ data }) => setStoryIds(data));
    }
  }

  return (
    <div className='index'>
      <div className='container'>
        <div className='top-head row'>
          <div className='new-btn'>
            <button
              className={classNames(
                'btn new-article',
                active === 'newarticle' ? 'active' : null
              )}
              onClick={() => setActiveArticle('newarticle')}
            >
              New Articles
            </button>
          </div>
          <div className='top-btn'>
            <button
              className={classNames(
                'btn top-article',
                active === 'toparticle' ? 'active' : null
              )}
              onClick={() => setActiveArticle('toparticle')}
            >
              Top Articles
            </button>
          </div>
          <div className='jobs-btn'>
            <button
              className={classNames(
                'btn job-article',
                active === 'job' ? 'active' : null
              )}
              onClick={() => setActiveArticle('job')}
            >
              Jobs
            </button>
          </div>
        </div>

        {storyIds.slice(0, count).map((storyId, index) => (
          <Articles key={storyId} storyId={storyId} index={index} />
        ))}

        <ScrollUpButton />
      </div>
    </div>
  );
};

export default Index;
