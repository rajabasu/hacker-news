import React, { useState, useEffect, memo } from 'react';
import { getStory } from '../../apicalls/HackerNewsApi';
import './Articles.scss';
import { mapTime } from '../../utils/mapTime';

const Articles = memo(function Story({ storyId, index }) {
  const [story, setStory] = useState({});
  useEffect(() => {
    getStory(storyId).then((data) => data && data.url && setStory(data));
  }, []);

  return story && story.url ? (
    <div className='articles row'>
      <div className='serial-no'>{index + 1}.</div>
      <div>
        <div className='title'>
          <a href={story.url} target='_blank'>
            {story.title}
          </a>
        </div>
        <div className='details row'>
          <div className='by mr-2'>
            By: <i>{story.by}</i> |
          </div>
          <div className='points mr-2'>
            Points: <i>{story.score}</i> |
          </div>
          <div className='time'>
            Posted: <i>{mapTime(story.time)}</i>
          </div>
        </div>
      </div>
    </div>
  ) : null;
});

export default Articles;
