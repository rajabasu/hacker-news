import axios from 'axios';

export const baseUrl = 'https://hacker-news.firebaseio.com/v0/';
export const newStoriesUrl = `${baseUrl}newstories.json`;
export const topStoriesUrl = `${baseUrl}topstories.json`;
export const jobStoryUrl = `${baseUrl}jobstories.json`;
export const storyUrl = `${baseUrl}item/`;

export const getStory = async (storyId) => {
  const result = await axios
    .get(`${storyUrl + storyId}.json`)
    .then(({ data }) => data);

  return result;
};

export const getTopStory = async (storyId) => {
  const result = await axios
    .get(`${storyUrl + storyId}.json`)
    .then(({ data }) => data);

  return result;
};

export const getJobStory = async (storyId) => {
  const result = await axios
    .get(`${storyUrl + storyId}.json`)
    .then(({ data }) => data);

  return result;
};

// =============================

export const getStoryIds = async () => {
  const result = await axios.get(newStoriesUrl).then((data) => data);

  return result;
};

export const getTopStoryIds = async () => {
  const result = await axios.get(topStoriesUrl).then((data) => data);

  return result;
};

export const getJobStoryIds = async () => {
  const result = await axios.get(jobStoryUrl).then((data) => data);

  return result;
};
