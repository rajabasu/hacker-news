import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.scss';
import { Route, Switch } from 'react-router-dom';
import Navbar from './components/Navbar/Navbar';
import Default from './components/Default/Default';
import Index from './components/Index/Index';

function App() {
  return (
    <React.Fragment>
      <Navbar />
      <Switch>
        <Route exact path='/' component={Index} />
        <Route component={Default} />
      </Switch>
    </React.Fragment>
  );
}

export default App;
